import sys
import re
import random
import pickle

confusion_set ={
		" your ":" you're ",
		" they're ":" their ",
		" their ":" they're ",
		" you're ":" your ",
		" it's ":" its ",
		" its ":" it's ",
		" loose ":" lose ",
		" lose ":" loose ",
		" to ":" too ",
		" too ":" to "
		}

substitutions =[	
		"your",
		"they're",
		"their",
		"you're",
		"it's",
		"its",
		"loose",
		"lose",
		"to",
		"too"
		]


def mix(sentence,tags):
	words = sentence.split()
	pos = tags.split()
	result = ""
	for i in range(len(words)):
		if words[i] in substitutions:
			result += words[i]+" "
		else:
			result += pos[i]+" "
	return result[:-1]


def validateText(sentences,table,transition):
	Result = list()
	for sentence in sentences:
		if(isSubstitutable(sentence)):
			tags1 = tagSentence(sentence,table)
			mixed1 = mix(sentence,tags1) 
			substituted = susbstitute(sentence)
			tags2 = tagSentence(substituted,table)
			mixed2 = mix(substituted,tags2) 
			P1 = transitionProbability(mixed1,transition)
			P2 = transitionProbability(mixed2,transition)
			if(P2>(P1*2)):
				Result.append(substituted)
			else:
				Result.append(sentence)
		else:
			Result.append(sentence)
	return Result
		
def transitionProbability(tagSequence,transition):
	
	grams = extractTrigrams(tagSequence)
	P=1

	#print("NEW")
	for gram in grams:
		tags = gram.split()
		term = "PLACE1:"+tags[0]+" PLACE2:"+tags[1]
		current_tag = tags[2]
		P = P * transition[term][current_tag]
	return P

def extractTrigrams(tagSequence):
	t1="TAG"
	t2="TAG"
	tags = tagSequence.split()
	grams = list()
	for tag in tags:
		gram = t2+" "+t1+" "+tag
		t2 = t1
		t1=tag
		grams.append(gram)
	return grams
		
def susbstitute(sentence):
	result=(sentence + '.')[:-1]
	for substitute in confusion_set:
		if substitute in sentence:
			result = result.replace(substitute,confusion_set[substitute])
	return result

def isSubstitutable(sentence):
	flag = False
	for substitute in confusion_set:
		if substitute in sentence:
			flag = True
	return flag

def shape(word):
	res=""
	capLetter = re.compile("[A-Z]")
	lowerCase =  re.compile("[a-z]")
	number =  re.compile("[0-9]")
	punct = re.compile("[^a-zA-Z0-9]")
	for sym in word:
		if lowerCase.match(sym):
			res = res +"a"
		if capLetter.match(sym):
			res = res +"A"
		if number.match(sym):
			res = res +"9"
		if punct.match(sym):
			res = res +"P"
	return res

def getEndings(word):
	return word[-3:].lower()

def sentSplit(sentence):#alg to split sentence
	result = sentence.split()
	return result
	
def tagSentence(sentence,table): 
	listOfGrams=[]
	nclass = set()
	buf=table['prev:BOS']
	for y in buf:
		nclass.add(y)

	bos="BOS"
	eos="EOS"
	pshape="PSHAPE"
	pword="PREVWORD"
	ending="333"
	taggedSentence=""
	words = sentSplit(sentence)
	if(len(words)>1):

		for i in range(len(words)-1):
			curshape = shape(words[i])
			features =" prev:"+bos+" current:"+words[i].lower()
			features += " currshape:"+curshape+" prevshape"+pshape+" prevend:"+ending+" prevword:"+pword
			features += " currend:"+getEndings(words[i])

			tag = tagFeatures(features,nclass,table)
			taggedSentence += tag+" "
			bos = tag				
			pshape=curshape
			ending=getEndings(words[i])
			pword=words[i].lower()


		curshape = shape(words[len(words)-1])
		features =" prev:"+bos+" current:"+words[len(words)-1].lower()
		features += " currshape:"+curshape+" prevshape"+pshape+" prevend:"+ending+" prevword:"+pword
		features += " currend:"+getEndings(words[len(words)-1])

		tag = tagFeatures(features,nclass,table)
		taggedSentence += tag+" "

	elif(len(words)==1):

		fbos="prev:"+bos
		fcurrent="current:"+words[0].lower()
		fnext="next:"+eos
		curshape = shape(words[0])
		features =" prev:"+bos+" current:"+words[0].lower()
		features += " currshape:"+curshape+" prevshape"+pshape+" prevend:"+ending+" prevword:"+pword
		features += " currend:"+getEndings(words[0])

		tag = tagFeatures(features,nclass,table)
		taggedSentence += tag+" "
	return taggedSentence

def tagFeatures(features, nclass, table):
		passResult = 0.0
		maxr = -100.0
		tag = "NN"
		F=[]
		for feature in features.split():
			if(feature in table):
				F.append(feature)
			else:
				F.append("unknown")

		for y in nclass:
			passResult = 0.0
			for f in F:			
				passResult+= table[f][y]
			if(maxr<passResult):
				maxr=passResult
				tag = y
		return tag



############_BEGIN_############

openfile = open( "STDIN", 'r')
writefile = open( "STDOUT", 'w')
modelfile = open(sys.argv[1],"rb")

model=pickle.load(modelfile)
sentences = openfile.readlines()
result = validateText(sentences,model[0],model[1])
for line in result:
	writefile.write(line)
modelfile.close()
openfile.close()
writefile.close()

############_END_############
