import sys
import re
import random 
import pickle


confusion_set ={
		"your":" you're ",
		"they're":" their ",
		"their":" they're ",
		"you're":" your ",
		"it's":"its",
		"its":"it's",
		"loose":"lose",
		"lose":"loose",
		"to":"too",
		"too":"to"
		}

def shape(word):
	res=""
	capLetter = re.compile("[A-Z]")
	lowerCase =  re.compile("[a-z]")
	number =  re.compile("[0-9]")
	punct = re.compile("[^a-zA-Z0-9]")
	for sym in word:
		if lowerCase.match(sym):
			res = res +"a"
		if capLetter.match(sym):
			res = res +"A"
		if number.match(sym):
			res = res +"9"
		if punct.match(sym):
			res = res +"P"
	return res

def splitter(word):
	tags=word.split("/")
	length=len(tags)
	last_tag=tags[length-1]
	middle_tag=tags[length-2]
	result=[middle_tag,last_tag]
	return result

def getEndings(word):
	return word[-3:].lower()

def getFeaturesData(sentences):
	listOfGrams=[]
	bugOfWords = set()
	nclass = set()
	dfeatures = set()
	for line in sentences:
		bos="BOS"
		eos="EOS"
		pshape="PSHAPE"# new feature!
		pword="PREVWORD"# new feature!
		ending="333"# new feature!
		words = line.split()
		for i in range(len(words)-1): 
	
			props = splitter(words[i])
			curshape = shape(props[0])# new feature!
			features =" prev:"+bos+" current:"+props[0].lower()
			features += " currshape:"+curshape+" prevshape"+pshape+" prevend:"+ending+" prevword:"+pword
			features += " currend:"+getEndings(props[0])
			listOfGrams.append(props[1]+features)
			
			pshape=curshape
			ending=getEndings(props[0])
			pword=props[0].lower()
			bos = props[1]
	
			nclass.add(props[1])
			for feature in features.split():
				dfeatures.add(feature)
	
		lwords = splitter(words[len(words)-1])
		curshape = shape(lwords[0])
		features =" prev:"+bos+" current:"+lwords[0].lower()
		features +=" currshape:"+curshape+" prevshape"+pshape+" prevend:"+ending+" prevword:"+pword
		features += " currend:"+getEndings(lwords[0])
		listOfGrams.append(lwords[1]+features)
		nclass.add(lwords[1])

		for feature in features.split():
			dfeatures.add(feature)
	results={}
	results[0]=listOfGrams
	results[1]=dfeatures
	results[2]=nclass
	return results

def initLearnModul(features_data):

	
	features = features_data[1]
	nclass = features_data[2]
	table = dict()
	avetable = dict()
	featChang = dict()
	featItr = dict()
	modul = dict()
	for f in features:
		classes = {}
		for y in nclass:
			classes[y] = 0.0
		table[f]=classes

	for f in features:
		featChang[f]=0
		featItr[f]=0

	for f in features:
		classes = {}
		for y in nclass:
			classes[y] = 1.0
		avetable[f]=classes


	modul[0] = table
	modul[1] = avetable
	modul[2] = featChang
	modul[3] = featItr
	return modul



def updateTable(features,nclass,avetable,table,featChang,truth,prediction):	
			
	for y in nclass:
		for j in range(1,len(features)):
			avetable[features[j]][y]=avetable[features[j]][y]+table[features[j]][y]*featChang[features[j]]	
	for j in range(1,len(features)):
		featChang[features[j]] = 1
		table[features[j]][truth] = table[features[j]][truth] + 1
		table[features[j]][prediction] = table[features[j]][prediction] - 1


def trainTagWord(modul,features_data):
	
	table = modul[0]
	avetable = modul[1]
	featChang = modul[2]
	featItr = modul[3]

	listOfGrams = features_data[0]
	dfeatures = features_data[1]
	nclass = features_data[2]
	errors = 0	

	for i in range(1):
		errors = 0	
		for tgram in listOfGrams:
			features = tgram.split()
			truth=features[0]
			passResult = 0
			maxr = -100.0
			prediction = ""

			for y in nclass:#iterate by classes			
				passResult=1.0
				for j in range(1,len(features)):
					passResult += table[features[j]][y]
				if(maxr<passResult):
					maxr=passResult
					prediction = y

			if prediction == truth:#agree
				for j in range(1,len(features)):
					featChang[features[j]] = featChang[features[j]]+1
			else:#disagree
				errors = errors + 1
				updateTable(features,nclass,avetable,table,featChang,truth,prediction)

			for j in range(1,len(features)):#assigning iteration
				featItr[features[j]]=featItr[features[j]]+1

		random.shuffle(listOfGrams)
		print("it =",i,"errors =",errors," number of iterations =",str(len(listOfGrams)*len(nclass)))	
	closeSession(dfeatures,nclass,avetable,featChang,table)
	print(" correct matches=",str(100-100*(errors/len(listOfGrams))), "errors =",errors,"correct =",str(len(listOfGrams)-errors))
	
	return toNoramalize(avetable,dfeatures,nclass,featItr)

def initTransitionProb(nclass):
	
	transition = dict()
	t="TAG"
	nclass.add(t)
	for subs in confusion_set:
		nclass.add(subs)
	
	for y1 in nclass:
		for y2 in nclass:
			term = "PLACE1:"+y1+" PLACE2:"+y2
			localprob = dict()
			for y in nclass:
				localprob[y] = 1
			transition[term]=localprob
	return transition


def trainTransition(sentences,transition):
	#len(sentences)
	for i in range(len(sentences)):		
		sentence = sentences[i]
		grams = trigramsPOS(sentence)
		integrateToTransition(grams,transition)
	normalizeTransition(transition)
	return transition


def normalizeTransition(transition):
	counter=dict()
	for term in transition:
		counter[term] = 0
		tab = transition[term]
		for tag in tab:
			counter[term] += transition[term][tag] 
	for term in transition:
		tab = transition[term]
		for tag in tab:
			transition[term][tag] = transition[term][tag]/(counter[term]+len(tab))


def integrateToTransition(grams,transition):
	for gram in grams:
		tags = gram.split()
		term = "PLACE1:"+tags[0]+" PLACE2:"+tags[1]
		current_tag = tags[2]
		transition[term][current_tag] += 1

def trigramsPOS(sentence):
	tags=list()
	grams = list()
	t1="TAG"
	t2="TAG"
	words = sentence.split()

	for tagword in words:
		word = tagword.split("/")[0]
		if word in confusion_set:
			tags.append(word)
		else:
			tags.append(tagword.split("/")[1])

	for tag in tags:
		gram = t2+" "+t1+" "+tag
		t2 = t1
		t1=tag
		grams.append(gram)

	return grams	


def closeSession(features,nclass,avetable,featChang,table):
	for f in features:#closing the last session
		for y in nclass:
			avetable[f][y]=avetable[f][y]+table[f][y]*featChang[f]
	classX={}
	for y in nclass:#adding error feature
		classX[y] = 0.0
	avetable["unknown"]=classX


def toNoramalize(avetable,features,nclass,featItr):	
	for f in features:#averaging
		for y in nclass:
			avetable[f][y] = avetable[f][y]/featItr[f]
	return avetable


##program starting
trainfile = open( sys.argv[1], 'r',errors='ignore')
modelfile = open(sys.argv[2],"wb")
sentences = trainfile.readlines() 
model=dict()

features_data = getFeaturesData(sentences)
modul = initLearnModul(features_data)
table = trainTagWord(modul,features_data)

transition = initTransitionProb(features_data[2])
transition = trainTransition(sentences,transition)

model[0]=table
model[1]=transition

pickle.dump(model,modelfile)
modelfile.close()
trainfile.close()
