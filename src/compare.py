import sys

substitutions =[	
		" your ",
		" they're ",
		" their ",
		" you're ",
		" it's ",
		" its ",
		" loose ",
		" lose ",
		" to ",
		" too "
		]

def compare(errortext,cleantext,testtext):
	false_positive=0
	true_positive=0
	false_negative=0
	true_negative = 0
	empty=0
	for i in range(len(errortext)):
		error = errortext[i]
		clean = cleantext[i]
		test  = testtext[i]
		if(error == clean):
			if(test != clean):
				false_negative += 1
			else: 
				flag = False
				for sub in substitutions:
					if sub in clean:
						flag=True
				if(flag):
					true_negative += 1
				else:
					empty += 1
		else:
			if(clean == test):
				true_positive += 1
			else:
				false_positive +=1
	print("false_negative",false_negative)
	print("true_negative",true_negative)
	print("true_positive",true_positive)
	print("false_positive",false_positive)
	print("empty",empty)
	print("errors =",(false_negative+false_positive))
	print("correct =",(true_negative+true_positive))
	print("relation =",((true_negative+true_positive)/((true_negative+true_positive)+(false_negative+false_positive))))

errfile = open(sys.argv[1],"r")
cleanfile = open(sys.argv[2],"r")
testfile = open(sys.argv[3],"r")

errortext = errfile.readlines()
cleantext = cleanfile.readlines()
testtext = testfile.readlines()

compare(errortext,cleantext,testtext)

errfile.close()
cleanfile.close()
testfile.close()
