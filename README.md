###AVERAGED PERCEPTRON + MIXED TRIGRAMS + BAYESIAN PROBABILITY TABLES###

Used sources: algorithm of averaged perceptron for POS tagging.

Davide Fossati, Barbara Di Eugenio. A Mixed Trigrams Approach for Context Sensitive Spell Checking.
Department of Computer Science, University of Illinois at Chicago, Chicago, IL, USA.
http://fossati.us/papers/fossati-cicling07.pdf


My approach is to compute the most probable confusion word for a given context.
I define sentence with confusion word and then compute probabilities of occuring 
similiar confusion words in a such surround context. Then choose the most probable one.
While representation of the sentence context is taken as a sequence of part of speech tags of the sentence


#Training Module:#
Using POS training data and averaged perceptron, we train perceptron to classify text.
Then we extract part of speech tag trigrams from sentences of POS training data  where 
the words of confusion set are met we replace pos tag of confusion word to itself word. 
For example  such combination of words 

    "revise its quotas"

would be  tagged as:

    VB PRP$ NNS

and then confusion word "its" would replace PRP$ (we replace only confusion words)

    VB its NNS 

next, we generate trigrams
(For the sentence of 3 words, 4 trigrams will be generated)

Further we use such trigrams in our program. We compute probability of such trigrams for 
our set of confusion words in the following way:
    
    P(trigram)=P(tag1 tag2 tag3)=Number(tag1 tag2 tag3)/Number(tag1 tag2 )

So we create Bayesian probabilities for set of trigrams.

Trained module consists of two learned tables

1) learned data for averaged perception to tag text

2) Bayesian probabilities table


#Text correction program:#

First program read sentence where confusion word is met, then using averaged perceptron program tag it. 
Again we extract part of speech tag trigrams from such sentences and replace pos tag to only respective 
confusion word as in the above. We also clone these trigrams except replacing original 
confusion word to proposed one from confusion set of words, homophones. Using Bayesian probabilities table 
we compute probability of each pair sentences and choose most probable one from each pair for the output.
Probability of a confusion word in the context of the sentence:

    P(confusion word|sentence)=P(trigram1)*P(trigram2)*...*P(trigramN)

#Program execution#

The training program, train.py, should run as follows:

    python3 train.py TRAININGFILE MODEL
    
MODELFILE is the output file containing the model.

The correct.py program should run as follows:

    python3 correct.py MODEL
    
where MODEL is the model generated by train.py
correct.py  takes its input from STDIN , and writes output to STDOUT,